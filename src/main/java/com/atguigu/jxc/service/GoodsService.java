package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> unitList();


    Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveOrUpdate(Goods goods);

    void deleteById(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    void deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();

    Goods getGoodsById(Integer goodsId);
}
