package com.atguigu.jxc.service;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();


    void save(String goodsTypeName, Integer pId);

    boolean delete(Integer goodsTypeId);
}
