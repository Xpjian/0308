package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.util.JSONs;
import com.atguigu.jxc.util.StringUtil;
import com.atguigu.jxc.vo.DamageListGoodsVo;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@Service
public class DamageServiceImpl implements DamageService {

    @Autowired
    DamageDao damageDao;

    @Override
    public void save(DamageList damageList, String damageListGoodsStr) {

        damageDao.saveDamageList(damageList);

        Integer damageListId = damageList.getDamageListId();

        List<DamageListGoodsVo> damageListGoodsVoList = JSONs.jsonStrToObj(damageListGoodsStr, new TypeReference<List<DamageListGoodsVo>>() {
        });
        for (DamageListGoodsVo damageListGoodsVo : damageListGoodsVoList) {
            DamageListGoods damageListGoods = new DamageListGoods();

            damageListGoods.setGoodsId(damageListGoodsVo.getGoodsId());
            damageListGoods.setGoodsCode(damageListGoodsVo.getGoodsCode());
            damageListGoods.setGoodsName(damageListGoodsVo.getGoodsName());
            damageListGoods.setGoodsModel(damageListGoodsVo.getGoodsModel());
            damageListGoods.setGoodsUnit(damageListGoodsVo.getGoodsUnit());
            damageListGoods.setGoodsNum(damageListGoodsVo.getGoodsNum());
            damageListGoods.setPrice(damageListGoodsVo.getPrice());
            damageListGoods.setTotal(damageListGoodsVo.getTotal());
            damageListGoods.setDamageListId(damageListId);
            damageListGoods.setGoodsTypeId(damageListGoodsVo.getGoodsTypeId());

            damageListGoods.setDamageListId(damageListId);
            damageDao.saveDamageListGoods(damageListGoods);
        }

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {

        Map<String, Object> map = new HashMap<>();

        if (!StringUtils.hasText(sTime) || !StringUtils.hasText(eTime)){
            map.put("", new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS));
            return map;
        }
        List<DamageList> damageLists = damageDao.list(sTime,eTime);

        map.put("rows",damageLists);

        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();

        if (damageListId == null){
            map.put("", new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS));
            return map;
        }
        List<DamageListGoods> damageListGoodsList = damageDao.goodsList(damageListId);

        map.put("rows",damageListGoodsList);

        return map;
    }
}
