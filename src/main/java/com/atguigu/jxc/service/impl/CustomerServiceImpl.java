package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Customer> supplierList = customerDao.list(rows,offSet,customerName);

        Long count = customerDao.getCount();

        map.put("total",count);
        map.put("rows",supplierList);

        return map;

    }

    @Override
    public void saveOrUpdate(Customer customer, String customerId) {
        if (customer != null){
            if (customerId != null && customerId != ""){
                customerDao.update(customer,customerId);
            }else {
                customerDao.save(customer);
            }
        }
    }

    @Override
    public void deleteByIds(String ids) {
        if (ids != null && ids != ""){
            String[] split = ids.split(",");
            for (String id : split) {
                customerDao.deleteByIds(id);
            }
        }
    }
}
