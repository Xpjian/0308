package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

         List<Supplier> supplierList = supplierDao.list(rows,offSet,supplierName);

         Long count = supplierDao.getCount();

         map.put("total",count);
         map.put("rows",supplierList);

        return map;
    }

    @Override
    public void saveOrUpdate(Supplier supplier, String supplierId) {
        if (supplier != null){
            if (supplierId != null && supplierId != ""){
                supplierDao.update(supplier,supplierId);
            }else {
                supplierDao.save(supplier);
            }
        }
    }

    @Override
    public void deleteByIds(String ids) {
        if (ids != null && ids != ""){
            String[] split = ids.split(",");
            for (String id : split) {
                supplierDao.deleteByIds(id);
            }
        }
    }
}
