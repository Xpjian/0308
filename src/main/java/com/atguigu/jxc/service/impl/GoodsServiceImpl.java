package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.vo.ListIncentoryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<ListIncentoryVo> goodsList = goodsDao.listInventory(rows, offSet, codeOrName, goodsTypeId);

        List<Goods> list = goodsList.stream().map(item -> {
            if (item.getSaleNum() == null) {
                item.setSaleNum(0);
            }
            if (item.getReturnNum() == null) {
                item.setReturnNum(0);
            }
            Goods resultList = new Goods();
            BeanUtils.copyProperties(item, resultList);
            resultList.setSaleTotal(item.getSaleNum() - item.getReturnNum());
            return resultList;
        }).collect(Collectors.toList());

        Long count = goodsDao.getGoodsCount(codeOrName, goodsTypeId);

        map.put("total", count);

        map.put("rows", list);


        return map;
    }

    @Override
    public Map<String, Object> unitList() {
        Map<String, Object> map = new HashMap<>();
        List<Unit> unitList = goodsDao.unitList();
        map.put("rows", unitList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Integer> goodsTypeIdList = new ArrayList<>();

        goodsTypeIdList = reGoodsTypeIdList(goodsTypeIdList, goodsTypeId);

        List<Goods> goodsList = goodsDao.goodsList(rows, offSet, goodsName, goodsTypeIdList);

        Long count = goodsDao.getGoodsCountByIdAndName(goodsName, goodsTypeIdList);
        Map<String, Object> map = new HashMap<>();
        map.put("total", count);

        map.put("rows", goodsList);


        return map;
    }

    private List<Integer> reGoodsTypeIdList(List<Integer> reGoodsTypeIds, Integer goodsTypeId) {
        if (goodsTypeId == null){
            return reGoodsTypeIds;
        }
        reGoodsTypeIds.add(goodsTypeId);
        List<GoodsType> childGoodsTypeList = goodsDao.getGoodsTypeById(goodsTypeId);
        if (childGoodsTypeList == null || childGoodsTypeList.size() == 0) {
            return reGoodsTypeIds;
        }
        for (GoodsType childGoodsType : childGoodsTypeList) {
            reGoodsTypeIds = reGoodsTypeIdList(reGoodsTypeIds, childGoodsType.getGoodsTypeId());
        }
        return reGoodsTypeIds;
    }

    @Override
    public void saveOrUpdate(Goods goods) {
        if (goods != null) {
            if (goods.getGoodsId() != null) {
                //修改
                goodsDao.updateById(goods);
            } else {
                //更新
                goodsDao.save(goods);
            }
        }
    }

    @Override
    public void deleteById(Integer goodsId) {
        goodsDao.deleteById(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(rows, offSet, nameOrCode);

        Long count = goodsDao.getGoodsCountNo(nameOrCode);

        map.put("total", count);

        map.put("rows", goodsList);

        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(rows, offSet, nameOrCode);

        Long count = goodsDao.getGoodsCountHas(nameOrCode);

        map.put("total", count);

        map.put("rows", goodsList);

        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.list();

        map.put("rows", goodsList);

        return map;
    }

    @Override
    public Goods getGoodsById(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        return goods;
    }
}
