package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowService;
import com.atguigu.jxc.util.JSONs;
import com.atguigu.jxc.vo.DamageListGoodsVo;
import com.atguigu.jxc.vo.OverFlowListGoodsVo;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/12
 * description :
 */
@Service
public class OverflowServiceImpl implements OverflowService {
    @Autowired
    OverflowDao overflowDao;

    @Override
    public void save(OverflowList overflowList, String overflowListGoodsStr) {
        overflowDao.saveOverflowList(overflowList);

        Integer overflowListId = overflowList.getOverflowListId();

        List<OverFlowListGoodsVo> overFlowListGoodsVoList = JSONs.jsonStrToObj(overflowListGoodsStr, new TypeReference<List<OverFlowListGoodsVo>>() {
        });
        for (OverFlowListGoodsVo overFlowListGoodsVo : overFlowListGoodsVoList) {
            OverflowListGoods overflowListGoods = new OverflowListGoods();

            overflowListGoods.setGoodsId(overFlowListGoodsVo.getGoodsId());
            overflowListGoods.setGoodsCode(overFlowListGoodsVo.getGoodsCode());
            overflowListGoods.setGoodsName(overFlowListGoodsVo.getGoodsName());
            overflowListGoods.setGoodsModel(overFlowListGoodsVo.getGoodsModel());
            overflowListGoods.setGoodsUnit(overFlowListGoodsVo.getGoodsUnit());
            overflowListGoods.setGoodsNum(overFlowListGoodsVo.getGoodsNum());
            overflowListGoods.setPrice(overFlowListGoodsVo.getPrice());
            overflowListGoods.setTotal(overFlowListGoodsVo.getTotal());
            overflowListGoods.setOverflowListId(overflowListId);
            overflowListGoods.setGoodsTypeId(overFlowListGoodsVo.getGoodsTypeId());

            BeanUtils.copyProperties(overFlowListGoodsVo, overflowListGoods);
            overflowListGoods.setOverflowListId(overflowListId);
            overflowDao.saveDamageListGoods(overflowListGoods);
        }
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {

        Map<String, Object> map = new HashMap<>();

        if (!StringUtils.hasText(sTime) || !StringUtils.hasText(eTime)){
            map.put("", new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS));
            return map;
        }
        List<OverflowList> overflowLists = overflowDao.list(sTime,eTime);

        map.put("rows",overflowLists);

        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();

        if (overflowListId == null){
            map.put("", new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS));
            return map;
        }
        List<OverflowListGoods> overflowListGoodsList = overflowDao.goodsList(overflowListId);

        map.put("rows",overflowListGoodsList);

        return map;
    }
}
