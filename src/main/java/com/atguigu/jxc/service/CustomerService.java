package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface CustomerService {
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Customer customer, String customerId);

    void deleteByIds(String ids);
}
