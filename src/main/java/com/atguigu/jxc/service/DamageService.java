package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface DamageService {
    void save(DamageList damageList, String damageListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
