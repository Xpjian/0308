package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface SupplierService {
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    void saveOrUpdate(Supplier supplier, String supplierId);

    void deleteByIds(String ids);
}
