package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/12
 * description :
 */
public interface OverflowService {
    void save(OverflowList overflowList, String overflowListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
