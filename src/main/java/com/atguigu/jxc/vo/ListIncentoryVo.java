package com.atguigu.jxc.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@NoArgsConstructor
@Data
public class ListIncentoryVo {
    private Integer goodsId;
    private String goodsCode;
    private String goodsName;
    private Integer inventoryQuantity;
    private Double lastPurchasingPrice;
    private Integer minNum;
    private String goodsModel;
    private String goodsProducer;
    private Double purchasingPrice;
    private String remarks;
    private Double sellingPrice;
    private Integer state;
    private String goodsUnit;
    private Integer goodsTypeId;
    private String goodsTypeName;
    private Integer saleNum;
    private Integer returnNum;

}
