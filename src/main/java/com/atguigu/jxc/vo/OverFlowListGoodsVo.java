package com.atguigu.jxc.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/12
 * description :
 */
@NoArgsConstructor
@Data
public class OverFlowListGoodsVo {

    @JsonProperty("goodsId")
    private Integer goodsId;
    @JsonProperty("goodsTypeId")
    private Integer goodsTypeId;
    @JsonProperty("goodsCode")
    private String goodsCode;
    @JsonProperty("goodsName")
    private String goodsName;
    @JsonProperty("goodsModel")
    private String goodsModel;
    @JsonProperty("goodsUnit")
    private String goodsUnit;
    @JsonProperty("lastPurchasingPrice")
    private Double lastPurchasingPrice;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("goodsNum")
    private Integer goodsNum;
    @JsonProperty("total")
    private Double total;
}
