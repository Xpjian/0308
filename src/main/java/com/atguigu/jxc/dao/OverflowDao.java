package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/12
 * description :
 */
public interface OverflowDao {
    void saveOverflowList(@Param("overflowList") OverflowList overflowList);

    void saveDamageListGoods(@Param("overflowListGoods") OverflowListGoods overflowListGoods);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
