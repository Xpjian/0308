package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface CustomerDao {
    List<Customer> list(@Param("rows") Integer rows, @Param("offSet") int offSet, @Param("customerName") String customerName);

    Long getCount();

    void update(@Param("customer") Customer customer, @Param("customerId") String customerId);

    void save(@Param("customer") Customer customer);

    void deleteByIds(@Param("id") String id);
}
