package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.vo.ListIncentoryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<ListIncentoryVo> listInventory(@Param("rows") Integer rows,
                                        @Param("offSet") int offSet,
                                        @Param("codeOrName") String codeOrName,
                                        @Param("goodsTypeId") Integer goodsTypeId);

    Long getGoodsCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    List<Unit> unitList();



    void updateById(@Param("goods") Goods goods);

    void save(@Param("goods") Goods goods);

    void deleteById(@Param("goodsId") Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("rows") Integer rows, @Param("offSet") int offSet, @Param("nameOrCode") String nameOrCode);

    Long getGoodsCountNo(@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("rows") Integer rows, @Param("offSet") int offSet, @Param("nameOrCode") String nameOrCode);

    Long getGoodsCountHas(@Param("nameOrCode") String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    List<Goods> list();

    void deleteStock(@Param("goodsId") Integer goodsId);

    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    List<GoodsType> getGoodsTypeById(@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> goodsList(@Param("rows") Integer rows, @Param("offSet") int offSet, @Param("goodsName") String goodsName, @Param("goodsTypeIdList") List<Integer> goodsTypeIdList);

    Long getGoodsCountByIdAndName(@Param("goodsName") String goodsName, @Param("goodsTypeIdList") List<Integer> goodsTypeIdList);
}
