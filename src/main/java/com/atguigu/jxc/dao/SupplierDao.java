package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface SupplierDao {
    List<Supplier> list(@Param("rows") Integer rows, @Param("offSet") int offSet, @Param("supplierName") String supplierName);

    Long getCount();


    void update(@Param("supplier") Supplier supplier, @Param("supplierId") String supplierId);

    void save(@Param("supplier") Supplier supplier);

    void deleteByIds(@Param("id") String id);
}
