package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
public interface DamageDao {


    void saveDamageList(@Param("damageList") DamageList damageList);

    void saveDamageListGoods(@Param("damageListGoods") DamageListGoods damageListGoods);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);
}
