package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    void udateStatus(@Param("pId") Integer pId, @Param("status") int status);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getById(@Param("goodsTypeId") Integer goodsTypeId);

    Long getCountByPId(@Param("pId") Integer pId);
}
