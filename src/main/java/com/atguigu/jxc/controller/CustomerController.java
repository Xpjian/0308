package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @PostMapping("/customer/list")
    public Map<String,Object> list(Integer page, Integer rows, String customerName){
        Map<String,Object> map = customerService.list(page,rows,customerName);
        return map;
    }

    @PostMapping("/customer/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "customerId",required = false) String customerId,
                                  Customer customer){
        customerService.saveOrUpdate(customer,customerId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/customer/delete")
    public ServiceVO delete(String ids){
        customerService.deleteByIds(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
