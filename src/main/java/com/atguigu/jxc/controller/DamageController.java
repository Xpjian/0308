package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@RestController
public class DamageController {

    @Autowired
    DamageService damageService;

    @PostMapping("/damageListGoods/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr, HttpSession session){
        if (damageList == null || !StringUtils.hasText(damageListGoodsStr) ){
            return new ServiceVO<>(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);
        }
        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageService.save(damageList,damageListGoodsStr);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/damageListGoods/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        Map<String,Object> map = damageService.list(sTime,eTime);
        return map;
    }

    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        Map<String,Object> map = damageService.goodsList(damageListId);
        return map;
    }
}
