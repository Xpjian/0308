package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/11
 * description :
 */
@RestController
public class SupplierController {


    @Autowired
    private SupplierService supplierService;

    @PostMapping("/supplier/list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName){
        Map<String,Object> map = supplierService.list(page,rows,supplierName);
        return map;
    }


    @PostMapping("/supplier/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "supplierId",required = false) String supplierId,
                                  Supplier supplier){
        supplierService.saveOrUpdate(supplier,supplierId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/supplier/delete")
    public ServiceVO delete(String ids){
        supplierService.deleteByIds(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

}
