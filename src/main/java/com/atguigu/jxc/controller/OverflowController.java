package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * title :
 * author : Jian Xipeng
 * date : 2023/9/12
 * description :
 */
@RestController
public class OverflowController {

    @Autowired
    OverflowService overflowService;


    @PostMapping("/overflowListGoods/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session){
        if (overflowList == null || !StringUtils.hasText(overflowListGoodsStr) ){
            return new ServiceVO<>(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);
        }
        User user = (User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overflowService.save(overflowList,overflowListGoodsStr);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        Map<String,Object> map = overflowService.list(sTime,eTime);
        return map;
    }

    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        Map<String,Object> map = overflowService.goodsList(overflowListId);
        return map;
    }

}
